# Brief summary

This is a dockerfile with aida and torquessh:

- aiida: AiiDA (using Django backend), connected to 

- TODO: db: a postgres DB, 
  
and with ssh keys preconfigured to connect to 

- torquessh: a machine with torque installed, where the user aiida on container
  aiida can connect already passwordless to app@torquessh.
  I reuse torquessh-base, and I install Quantum ESPRESSO

# How to start everything

Run the script startup_firsttime.sh (only once), that will also
generate ssh keys and passwords, start the services, and setup AiiDA.


Currently, after starting everything you need to do

``docker exec -it aiida bash``

if you want to connect and use the machine directly.

# To shutdown everything

Use `./cleanup.sh`.



