#!/bin/bash
# Stop the script as soon as one step fails


set -e

#### This script takes care of starting up the full environment

## Step 1. Create passwords and credentials
if [ -e compose-init/sshkeys ]
then
    echo "Folder init/sshkeys exists already. I assume you already created"
    echo "the keys once, and I will reuse the content of the init folder."
    echo "If this is not the case, remove the init/sshkeys folder, and rerun"
    echo "this script."
fi
cd compose-init && ./run_once_generate_passwords.sh && cd ..
cd ..
## Step 2. Changed docker compose to docker. Start the full set of images. I also rebuild (this is not needed
## in general). Note that I start daemonized
#docker-compose up -d --build
docker build -t aiida .
docker run --name aiida -d -v aiidalogs:/home/aiida/.aiida -v aiidarepo:/home/aiida/.aiida_repo -v /$(pwd)/compose-init/sshkeys/sharedfolder:/home/aiida/.ssh/keys:ro --env-file /$(pwd)/compose-init/env/torque.env --env-file /$(pwd)/compose-init/env/aiida.env aiida
## Step 3. Setup postgres
## According to how I generated the aiida image, this will have already set
##
## 
docker pull postgres
docker run --rm --name db --env-file /$(pwd)/compose-init/env/postgres.env -d -p 5432:5432 -v dbdata:/var/lib/postgresql/data postgres


#docker exec aiida /bin/bash -l -c "/home/aiida/.dockerscripts/core/aiida_setup.sh"


### PLUGIN-SPECIFIC SECTION

# Setup also the code
#docker exec aiida /bin/bash -l -c "cat /home/aiida/.dockerscripts/plugin/code-setup-input.txt | verdi code setup"


