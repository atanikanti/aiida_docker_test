FROM aiidateam/torquessh_base:1.0

# Install required packages
CMD ["/sbin/my_init"]

RUN apt-get update \ 
    && apt-get install -y \
    openmpi-bin \
    quantum-espresso \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean all


# Use baseimage-docker's init system.

ENV HOME /root

CMD ["/sbin/my_init"]

# TODO: probably postgresql-server-dev-9.5 are needed only during
# the pip install phase, so could be removed afterwards (and maybe
# used in the same layer)

# install required software


RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install \
    mlocate \
    git \
    openssh-client \
    postgresql \
    postgresql-server-dev-all \
    python2.7 \
    && apt-get -y install \
    python-pip \
    ipython \
    python2.7-dev \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean all \
    && updatedb

# update pip and setuptools, required for AiiDA
RUN pip install -U pip setuptools

# add USER (no password)
RUN useradd -m -s /bin/bash aiida
#RUN chown aiida /home/aiida
##########################################
############ Installation Setup ##########
##########################################

# install rest of the packages as normal user

#RUN chmod -R 777 /home/aiida
#USER aiida
# set $HOME, create git directory
#ENV HOME /home/aiida

#RUN mkdir -p $HOME/code/
ENV HOME /home/aiida
RUN mkdir -p $HOME/code/
WORKDIR /home/aiida/code



## Get latest release from git
RUN git clone https://github.com/aiidateam/aiida_core.git && \
    cd aiida_core && \
     git checkout v0.12.2 && \
    cd ..

## Alternatively, use wget
#RUN wget --no-check-certificate -q \
#      https://github.com/aiidateam/aiida_core/archive/develop.tar.gz && \
#    tar xzf develop.tar.gz && \
#    rm develop.tar.gz && \
#    mv aiida_core-develop aiida_core

WORKDIR /home/aiida
# make ssh dir and create host entry for bitbucket.org
RUN mkdir $HOME/.ssh/ && \
    touch $HOME/.ssh/known_hosts

# verdi auto-complete to bashrc - currently disabled
#RUN echo 'eval "$(verdi completioncommand)"' >> $HOME/.bashrc 

# Add the bin folder to the path (e.g. for verdi) so that
# it works also from non-login shells
RUN echo 'export PATH=~/.local/bin:$PATH' >> $HOME/.bashrc

# Install AiiDA
WORKDIR /home/aiida/code/aiida_core
RUN pip install -U pip wheel setuptools --user && pip install -e . --user --no-build-isolation

# Important to end as user root!
USER root

# Make directory (if COPY in the line below creates it automatically, it then will
# belong to root and therefore you have permission problems)
RUN mkdir /home/aiida/.dockerscripts

## Copy startup scripts, will be used later
COPY ./scripts/ /home/aiida/.dockerscripts/

# Prepare the folders - the next command will assign it to the correct
# user, so it works with proper permissions when I mount 
# an external named volume
RUN mkdir ~/.aiida/ && mkdir ~/.ssh/keys && mkdir ~/.aiida_repo

#USER aiida
WORKDIR /home/aiida/code

## Get the 'aiida-qe' tagged version with git
RUN git clone https://github.com/aiidateam/aiida-quantumespresso.git && \
    cd aiida-quantumespresso && \
    git checkout release_v2.1.1 && \
    pip install --user -e . && \
    /home/aiida/.local/bin/reentry scan && \
    cd ..

# Make  sure all things created have the right permissions
USER root
RUN chown -R aiida:aiida /home/aiida/.dockerscripts && \
    chown -R aiida:aiida /home/aiida/.aiida && \
    chown -R aiida:aiida /home/aiida/.aiida_repo && \
    chown -R aiida:aiida /home/aiida/.ssh 

# TO CHECK: also because we need to start the web service and install Apache
# expose https port (for rest? or maybe instead expose jupyter?)
#EXPOSE 443

# Important to end as user root (to run the init script)
USER root

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]
